package com.lms.listner;

import java.util.Map;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lms.config.IApplicationService;
import com.lms.events.BaseEvent;
import com.lms.exception.DataIntegrityFailureException;

import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class LmsListner {

	@Autowired
	private ObjectMapper objectMapper;

	@Resource(name = "applicationServiceMap")
	private Map<String, IApplicationService> applicationServiceMap;

	@JmsListener(destination = "couse-queue-in")
	public void onReceive(@Payload final String lmsMessage) throws JsonMappingException, JsonProcessingException {
		try {
		if ("".equals(lmsMessage)) {

			throw new DataIntegrityFailureException("Payload cannot be empty");
		}

		final TypeReference<BaseEvent> baseEventReference = new TypeReference<BaseEvent>() {};
		final BaseEvent baseEvent = objectMapper.readValue(lmsMessage, baseEventReference);
		final IApplicationService service = applicationServiceMap.get(baseEvent.getBaseHeader().getEventName());
		if (Objects.nonNull(service)) {

			service.process(baseEvent);
		} else {
			log.error("service not found with event:{}",baseEvent.getBaseHeader().getEventName());
			throw new DataIntegrityFailureException(
					"service not found with Header : " + baseEvent.getBaseHeader().getEventName());
		}
	
		}
		catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

}
		
	}
