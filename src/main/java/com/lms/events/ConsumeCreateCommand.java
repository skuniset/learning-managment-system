package com.lms.events;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ConsumeCreateCommand {

	private BaseHeader baseHeader;
	private  CourseNodeV1 eventBody;

}
