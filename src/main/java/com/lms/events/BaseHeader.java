package com.lms.events;

import java.time.OffsetDateTime;
import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class BaseHeader {

	private String eventName;
	private String token;
	private OffsetDateTime eventTime;
	private UUID transactionUuid;
	private UUID corelationUuid;
	
}
