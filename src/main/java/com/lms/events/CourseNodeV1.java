package com.lms.events;


import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class CourseNodeV1{
	
	private UUID couseUuid;
	private String courseName;
	private String technology;
	private UUID technologyUuid;
	private Long courseDuration;
	private String URL;
	private String courseDescription;
}
