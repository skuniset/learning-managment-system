package com.lms.domainservice;


import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lms.entity.Course;
import com.lms.entity.Technology;
import com.lms.events.ConsumeCreateCommand;
import com.lms.events.CourseNodeV1;
import com.lms.exception.DataIntegrityFailureException;
import com.lms.repository.CourseRepository;
import com.lms.repository.TechnologyRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@Transactional
public class CreateCouseDomainService {

	@Autowired
	private CourseRepository courseRepository;

	@Autowired
	private TechnologyRepository technologyRepository;

	public void onCommand(final ConsumeCreateCommand command) {
		
		System.out.println(command.getEventBody().getCouseUuid());
		CourseNodeV1 eventBody = command.getEventBody();

		Course course = new Course();
		course.setCourseUuid(eventBody.getCouseUuid());

		Optional<Technology> optionalTechnology = technologyRepository.findById(eventBody.getTechnologyUuid());

		if (optionalTechnology.isPresent()) {

			course.setTechnology(optionalTechnology.get());

		} else {

			log.error("technology not found with technologyUuid:{}", eventBody.getTechnologyUuid());
			throw new DataIntegrityFailureException("technology not found with technologyUuid"+ eventBody.getTechnologyUuid());
		}

	}

}
