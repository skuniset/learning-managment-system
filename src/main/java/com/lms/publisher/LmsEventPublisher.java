package com.lms.publisher;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.model.MessageAttributeValue;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lms.events.BaseEventError;
import com.lms.events.BaseHeader;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class LmsEventPublisher {
	
	@Value("${aws.topicarn}")
	private String topicArn;

	@Autowired
	private AmazonSNS snsClinet;

	private ObjectMapper objectMapper;

	public PublishResult publishIdsEvents(final String messageAsJson, BaseHeader baseHeader,
			BaseEventError baseEventErrors) {

		if (Objects.nonNull(baseEventErrors)) {

			log.info("Event being published from {} with metadata as {} {}", "LMS", baseHeader, baseEventErrors);
		} else {
			log.info("Event being published from {} with metadata as {}", "LMS", baseHeader);
		}

		log.debug("publishing message to the outbound topic for eventName:{} " + ", transactionUuid:{} and message:{}",
				baseHeader.getEventName(), baseHeader.getTransactionUuid(), messageAsJson);

		final Map<String, MessageAttributeValue> messageAttributes = getMessageAttributes(baseHeader);
		final PublishRequest request = new PublishRequest().withMessageAttributes(messageAttributes)
				.withMessage(messageAsJson)
				.withTopicArn(topicArn);
		
		PublishResult result = snsClinet.publish(request);
		return result;

	}

	private Map<String, MessageAttributeValue> getMessageAttributes(BaseHeader baseHeader) {

		final Map<String, MessageAttributeValue> attributes = new HashMap<>();
		attributes.put("eventName",
				new MessageAttributeValue().withDataType("String").withStringValue(baseHeader.getEventName()));
		return attributes;
	}

}
