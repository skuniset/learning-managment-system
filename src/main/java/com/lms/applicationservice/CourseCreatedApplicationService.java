package com.lms.applicationservice;

import static com.lms.constants.LmsConstants.CREATE_COURSE;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lms.config.IApplicationService;
import com.lms.domainservice.CreateCouseDomainService;
import com.lms.events.BaseEvent;
import com.lms.events.ConsumeCreateCommand;
import com.lms.events.CourseNodeV1;
import com.lms.exception.DataIntegrityFailureException;

@Service
public class CourseCreatedApplicationService implements IApplicationService {

	private static final String EVENTNAME = CREATE_COURSE;

	@Autowired
	private ObjectMapper mapper;
	
	@Autowired
	private CreateCouseDomainService createCouseDomainService;

	@Override
	public void process(final BaseEvent baseEvent) {

		try {

			CourseNodeV1 courseDetails = mapper.readValue(baseEvent.getEventBody(), CourseNodeV1.class);

			ConsumeCreateCommand command = ConsumeCreateCommand.builder().baseHeader(baseEvent.getBaseHeader())
					.eventBody(courseDetails).build();
			
			createCouseDomainService.onCommand(command);
			

		} catch (JsonProcessingException e) {
			
			throw new DataIntegrityFailureException("unable to consume the message "+ e);
		}
	}

	@Override
	public String getServiceIdentifier() {

		return EVENTNAME;
	}

}
