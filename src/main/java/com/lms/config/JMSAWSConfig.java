package com.lms.config;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.EnableJms;

@Configuration
@EnableJms
public class JMSAWSConfig {

	@Value("${aws.region}")
	private String region;

	List<IApplicationService> iapplicationServices;

	@Autowired
	public JMSAWSConfig(final List<IApplicationService> iapplicationServices) {
		this.iapplicationServices = iapplicationServices;
	}

	@Bean(name = "applicationServiceMap")
	public Map<String, IApplicationService> getApplicationServiceMap() {

		return iapplicationServices.stream()
				.collect(Collectors.toMap(IApplicationService::getServiceIdentifier, Function.identity()));
	}

}
