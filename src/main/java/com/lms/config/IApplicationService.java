package com.lms.config;

import com.lms.events.BaseEvent;

public interface IApplicationService {

	public void process (final BaseEvent baseEvent);
	public String getServiceIdentifier();
}
