package com.lms.constants;

import lombok.experimental.UtilityClass;

@UtilityClass
public class LmsConstants {

	public static final String CREATE_COURSE = "CreateCourse";
	public static final String DELETE_COURSE = "DeleteCourse";
	public static final String UPDATE_COURSE = "UpdateCourse";
	public static final String COURSE_CREATED = "CourseCreated";
	public static final String COURSE_DELETED = "CourseDeleted";
	public static final String COURSE_UPDATED = "CourseUpdated";

}
