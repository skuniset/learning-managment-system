package com.lms.exception;

public class DataIntegrityFailureException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	public DataIntegrityFailureException(String message) {
		super("Data exception "+ message);
	}

}
