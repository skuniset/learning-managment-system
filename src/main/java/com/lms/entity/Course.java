package com.lms.entity;


import java.util.UUID;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "course")
@Data
@EqualsAndHashCode
public class Course {
	@Id
	@Column(name = "course_uuid")
	private UUID courseUuid;
	@Column(name = "course_name")
	private String courseName;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "technologyUuid")
	private Technology technology;
	@Column(name = "course_duration")
	private Long courseDuration;

}
