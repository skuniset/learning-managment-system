package com.lms.entity;


import java.util.UUID;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name="technology")
@Data
@EqualsAndHashCode
public class Technology {
	
	@Id
	@Column(name="technology_uuid")
	private UUID technologyUuid;
	
	@Column(name="technology_name")
	private String technologyName;
	
}
