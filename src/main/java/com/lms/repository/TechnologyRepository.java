package com.lms.repository;

import java.util.UUID;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.lms.entity.Technology;

@Repository
public interface TechnologyRepository extends CrudRepository<Technology, UUID>{

}
